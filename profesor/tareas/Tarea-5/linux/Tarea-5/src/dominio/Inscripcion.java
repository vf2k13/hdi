package dominio;

/**
 * Clase para inscripciones escolares
 * @author vic2k110
 */
public class Inscripcion {
    
    private String estudiante;
    private String semestre = "0";
    private String materia_1;
    private String materia_2;
    private String materia_3;
    private String taller;
    private String idioma;

    public Inscripcion() {
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getMateria_1() {
        return materia_1;
    }

    public void setMateria_1(String materia_1) {
        this.materia_1 = materia_1;
    }

    public String getMateria_2() {
        return materia_2;
    }

    public void setMateria_2(String materia_2) {
        this.materia_2 = materia_2;
    }

    public String getMateria_3() {
        return materia_3;
    }

    public void setMateria_3(String materia_3) {
        this.materia_3 = materia_3;
    }

    public String getTaller() {
        return taller;
    }

    public void setTaller(String taller) {
        this.taller = taller;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
    
    
    
    
    
}
