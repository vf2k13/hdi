package control;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import dominio.Inscripcion;
import validator.InscripcionValidator;
import servicio.InscripcionService;

// Clase que responde a la peticin hecha a /userRegistration.htm  
@Controller
@RequestMapping( "/InscripcionRegistration.htm" ) 
//Atributo que guarda información durante la sesión del usuario, en nuestro caso
//  nos importa que la inf. de la inscripción se guarde y se vaya pasando de página
//  a página
@SessionAttributes("inscripcion")

public class InscripcionController {
        
        //Interfaz para poder insertar al usuario a la base de datos (implementación en UserServiceImpl)
        private InscripcionService inscripcionService;
        //Clase que permite validar los datos del usuario (a diferencia de InscripcionService no es una interfaz)
        private InscripcionValidator inscripcionValidator;
 
        //La etiqueta Autowired hace que se vaya al "repositorio para instanciar desde ahí los
        //  parámetros recibidos" 
	@Autowired
	public InscripcionController( InscripcionService inscripcionService, InscripcionValidator inscripcionValidator ) {
            //Los parametros recibidos fueron instanciados en un principio en dispatcher-servlet.xml
            
            //Instancia para agregar usuario a la base de datos
            this.inscripcionService = inscripcionService;
            //Instancia que permite validar los datos del usuario
            this.inscripcionValidator = inscripcionValidator;
	}
	
        // Respuesta GET para /userRegistration.htm
        // Método cuando la peticion no viene por formulario
	@RequestMapping( method = RequestMethod.GET )
	public String showInscripcionForm( ModelMap model, HttpServletRequest request ) {
		//User user = new User();
                //Cargamos un usuario en blanco al model para ser llenado por
                // el usuario
		//model.addAttribute( "user", user ); //Se mapean los atributos para ser llenados
		//PRUEBA. Cambiamos a user por nuestro bean
                Inscripcion inscripcion = new Inscripcion();
                model.addAttribute( "inscripcion", inscripcion ); //Se mapean los atributos para ser llenados
                //Como la petición no viene por formulario, mostramos la
                // vista de registro userForm.jsp
		return "InscripcionForm"; //Se envía por la misma forma, no es necesario especificar el .jsp
	}
        
        // Respuesta POST para /userRegistration.htm
	@RequestMapping( method = RequestMethod.POST )
	public String onSubmit( @ModelAttribute("inscripcion") Inscripcion inscripcion, BindingResult result , HttpServletRequest request ) 
	 {
            //Recibimos por los parámetros los datos del usuario ya llenos
            //Con @ModelAttribute obtenemos los datos del atributo usuario
            //  (este atributo lo lleno el usuario) y los guardamos
            //  en el objeto user
            //BindingResult es para manejar los errores 
            //HttpServletRequest request petición que envía el cliente

                //Cadena que contendrá la vista de respuesta dependiendo el caso
                String formulario = "";
                
                //Insertamos al usuario a la base de datos
                //userService.add( user );

                //Insertamos la inscripción a la base de datos
                inscripcionService.add( inscripcion );
                //Redirigimos al formulario userSuccess.jsp
                formulario = "redirect:inscripcionSuccess.htm"; //este formato es para hacer uso del framework

                //Devolvemos la cadena formulario con la vista .jsp
                return formulario;

	}
}

