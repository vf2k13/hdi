### ¿Cómo ejecutar el proyecto?
- En NetBeans ir a file, luego a new project y Seleccionar como categoría "Java with Ant/Java Web" y en proyecto seleccionar "Web Aplication with Existing Source"

- Se selecciona la carpeta Tarea-5 como proyecto.

- Una vez importado el proyecto, se da clic derecho sobre el proyecto,después se procede a compilar y limpiar el proyecto.

- Crear la base de datos con las especificaciones del archivo /Tarea-5/src/servicio.properties

- Alternativamente se puede ejecutar el archivo .yml para correr la base de datos sobre un contenedor. 

- Generar la tabla de la base de datos con el script BD Tarea-5/Formulario.sql 

- En NetBeans dar clic derecho sobre el proyecto y ejecutar con run


### ¿Cómo correr el contenedor por primera vez para la base de datos?
- Necesitamos tener instalado docker y docker compose. Para docker compose sólo hay que descargar unos archivos y moverlos a ua carpeta en específico Tutorial: https://docs.docker.com/compose/install/

- Ejecutar docker-compose-yml con el siguiente comando en shell
```sh
docker-compose up
```

- Si no queremos ver los logs y que se nos libere la terminal ponemos 
```sh
docker-compose up -d
```

### ¿Cómo conectarse al contenedor desde línea de comandos?
- Listar los contenedores
```sh
docker container ls -a
```

- Conectarse al contenedor de acuerdo a su PID
```sh
docker exec -ti [PID DEL CONTENEDOR] /bin/bash
```

### Otros comandos útiles para docker
- volver a iniciar contendor con estado exit
```sh
docker start [PID DEL CONTENEDOR]
```


### ¿Cómo hacer la conexión de Workbrench a la base de datos y correr el .sql?
1. Correr el contenedor con la base de datos
2. Abrir Workbrench
3. Ir a MySQL Connections y dar clic en el símbolo + para crear una nueva conexión
4. Poner cualquier nobre a la conexión
5. En Hostname poner localhost
6. En el puerto poner del contenedor, que en nuetro caso sería el 32132
7. Probar la conexión dando clic en Test Connection e ingresar la contraseña de la base de datos (especificada en el .yml)
8. dar clic en ok
9. Ir a file y abrir sql sript
10.  Seleccionar todas las líneas con ctr-a y dar clic en el rayito para ejecutar las sentencias del .sql
11.  Verificar en el apartado de de la base de datos que estén nuestras tablas. Adicionalmente se puede hacer esta comprobación entrando al contenedor y haciendo la consulta por línea de comandos.


### ¿Cómo operar sobre MySQL desde línea de comandos?
**Aquí se supone que se está dentro del contenedor**
Iniciar línea de comandos de MySQL
```sh
mysql -u root -p test
```
**Contraseña:** admin

Mostrar todas las bases de datos
```sh
show databases;
```

Usar una base de datos
```sh
use NombreBaseDatos;
```

Mostrar todas las tablas
```sh
show tables;
```

Ver todas las tuplas de una tabla
```sh
select * from nombreTabla;
```



### ¿Cómo importar este proyecto a netbeans?
- Borrar todo lo relacionado a archivos de configuración para netbeans


### Comandos útlies de Git
- Obtener actualizar nuevos cambios a mi rama de trabajo
	- Supongamos que tenemos una rama padre y una rama hija

	- En padre tenemos el proyecto principal y en hija la usamos sólo para la parte gráfica

	- En algún punto padre tiene mejoras en la base de datos, pero hija no tiene esas mejoras, sólo tiene cambios en la parte gráfica, si quisieramos que hija tuviese esas nuevas mejoras por alguna razón, sería necesario pararnos en hija y hacer la fusión padre->hija ()
```sh
git merge padre
```

- Ver diferencias entre la rama actual y una rama a fusionar
	- Nos paramos en nuestra rama actual y ponemos git diff [Rama con la cual comparar] 
- 

### Consideraciones importantes
- El proyecto no funciona sin el JDK 8

- La base de datos se crea en el .sql con el nombre test

- La contraseña de la base de datos es admin y el usuario es admin ó root

- Primero correr el contenedor de la base de datos para que la aplicación funcione, luego hacer la conexión con la base de datos y correr /FormularioDBSpring3MVC/BDFormulario.sql para tener la base de datos completamente lista con las tablas

- La base de datos no tiene datos

- Se modificó el puerto del archivo Formulario/DBSpring3MVC/src/servicio.properties por el 32132 que es el que tiene mapeado el contenedor de la base de datos.

- De momento no se puede escribir en la tabla usuarios porque hay una pequeña falla con el nombre de la tabla usuario, parace ser que se hace un query usando U en lugar de u para esta tabla

Cómo abrir y correr el proyecto en Netbeans
	Abrir proyecto en Netbeans
		Ir a File

		Clic en Open project y seleccionar el proyecto Tarea 5

	Correr proyecto
		Levantar la base de datos
			Corremos el contenedor para la base de datos y así tener nuestra base de datos hecha. La base de datos se llama test
				docker-compose up -d

			Nos metemos a la base de datos para asegurarnos que funciona
				docker exec -ti [PID DEL CONTENEDOR] /bin/bash

				Dentro del contenedor ingresar
					mysql -u root -p test

			De manera alternativa, podemos comprobar el funcionamiento de la base de datos por medio de Workbrench si es que ya tenemos nuestra conexión hecha en Workbrench
				Abrimos Workbench

				Damos clic sobre la conexión

				Metemos la contraseña de la base de datos, la cual es admin

				Hacemos la siguiente query para verificar que todo está en orden
					select * from inscripcion;



		Crear las tablas de la base de datos
			Ir al apartado ¿Cómo hacer la conexión de Workbrench a la base de datos y correr el .sql? para correr el .sql y correr la base de datos

			Dentro del contenedor podemos consultar las tablas creadas ó también podemos volver a workbench y refrescar el menú izquierdo de tabals para visualizar las nuesvas tablas



		Clic derecho sobre el proyecto y darle en clean and build

		Clic derecho sobre el proyecto y dar clic en run	


######################################################3
	Abrir proyecto
		File
		New Project

		Seleccioner 
			Categories 
				Java with Ant/Java Web 

			Projects
				Web Aplication with Existing Source

		Next

		Seleccionamos dónde está ubicada la carpeta del proyecto, en nuestro caso está en /hdi/profesor/tareas/Tarea-5/linux/Tarea-5



	Correr proyecto



