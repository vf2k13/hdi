<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>Pagina de Registro</title>
   
   <!-- Especificaci�n del estilo para los errores -->
   <style>
     .error {
        color: #ff0000;
        font-style: italic;
      }
   </style>
   
   
   <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />           
   <!-- Importamos el componente dentro de la cabecera. Importamos desde internet -->
   <script type="text/javascript" 
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
   <script type="text/javascript" 
		src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>  	  
  
   <!--Hacemos una referencia al id datepicker y asociamos el componente .datepicker() -->
    <script>

  $(function() {

    $( "#datepicker" ).datepicker();

  });
    
  </script>
   
   
  <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />           
  <script type="text/javascript" 
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script type="text/javascript" 
		src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>  	
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    var availableTags = [
      "Baile",
      "Ajedrez",
      "Electronica",
      "Redaccion",
      "Oratoria",
      "Debate",
      "Teatro",
      "Lucha libre",
      "Coro",
      "Musica",
      "Arquitectura",
      "Guitarra",
      "Bateria",
      "Poesia"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  });
  </script>
   
  <!-- Carrucel donde usamos pageContext.request.contextPath para acceder a Tarea-5/WebContent y sus archivos-->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/website.css">
<!--<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>	-->
<script src="${pageContext.request.contextPath}/js/jquery.tinycarousel.min.js"></script>


<!-- Validaci�n -->
<!--
<script type = "text/javascript" src = "${pageContext.request.contextPath}jquery.js"> </script> 
<script type = "text/javascript" src = "${pageContext.request.contextPath}Jquery Validacion/dist/jquery.validate.js"> </script> 
-->

<!--Mapa-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>



<!-- Funci�n que inicia de inmediato los dos componentes -->
<script type="text/javascript">
    $(document).ready(function(){
    //Mapa
    var mapa;
    var latitud = parseFloat($('#v_latitud').val());
    var longitud = parseFloat($('#v_longitud').val());
    var marcador;
    var coordenadas = new google.maps.LatLng(latitud, longitud);
    var opciones = {
      center: coordenadas,
      zoom:18,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    mapa = new google.maps.Map($('#mapa')[0], opciones);
    ponCoordenadas(coordenadas);
    google.maps.event.addListener(mapa, 'click', function(event) {
      ponCoordenadas(event.latLng);
    });
    function ponCoordenadas(location) {
      if (marcador) {
          marcador.setPosition(location);
      } else {
          marcador = new google.maps.Marker({
            position: location,
            draggable: true,
            title: 'Arrastrar para marcar nuevas coordenadas.',
            map: mapa
          });
      }
    }
    google.maps.event.addListener(mapa, 'click', function(event) {
      ponCoordenadas(event.latLng);
      jQuery('#v_latitud').val(marcador.getPosition().lat());
      jQuery('#v_longitud').val(marcador.getPosition().lng());          
    });
    $("#b_reset").click(function(){location.reload();});
        
    //Carrucel
    $("#slider1").tinycarousel();

    //Validacion
    $('#v_codigo').focus();
    $("#form1").validate();
    
   
   
   

    });
</script>

<!-- Estilo para Mapa-->
<style>
    div#mapa {width: 500px;height:500px;}
    .cbr     {width: 500px; height:50px;}
</style>


</head>


<body>
    
    
    
<form:form method="POST" commandName="inscripcion">
	<table border="0" bgcolor="11BBB0">	
		<tr>		 
			<td colspan="3">
			  <center>
			    <h1>Formulario de Inscripci�n escolar al semestre 2022-1</h1>
                            <h3>Fotos y ubicaci�n de la Facultad de Ciencias</h3>
			  </center>
			</td>		 
		</tr>
                <tr>		 
			<td colspan="0">
			  <center>
                                
			        <!-- Ponemos carrucel -->
                                <div id="slider1">
                                    <a class="buttons prev" href="${pageContext.request.contextPath}#">left</a>
                                    <div class="viewport">
                                        <ul class="overview">
                                        <li><img src="${pageContext.request.contextPath}/images/picture6.jpg" /></li>
                                        <li><img src="${pageContext.request.contextPath}/images/picture5.jpg" /></li>
                                        <li><img src="${pageContext.request.contextPath}/images/picture4.jpg" /></li>									
                                        <li><img src="${pageContext.request.contextPath}/images/picture3.jpg" /></li>
                                        <li><img src="${pageContext.request.contextPath}/images/picture2.jpg" /></li>
                                        <li><img src="${pageContext.request.contextPath}/images/picture1.jpg" /></li>
                                        </ul>
                                    </div>
                                    <a class="buttons next" href="${pageContext.request.contextPath}#">right</a>
                                </div>
                                
			  </center>
			</td>
                        <td>
                            <!-- Ubicaci�n de la facultad-->
                            <table border=1 bgcolor="#58FAF4">
                                  <tr><td colspan=2><div id="mapa"></div></td></tr>  
                                  <tr>
                                  <tr>
                                    <td>
                                      <label>Latitud</label>
                                      <input type="text" value="19.324299740415146" id="v_latitud" disabled/>
                                    </td>
                                    <td>
                                      <label>Longitud</label>
                                      <input type="text" value="-99.17906917725963" id="v_longitud" disabled/>
                                    </td>
                                  </tr>  
                                    </tr>
                            </table>
                        </td>
		</tr>
                
		<tr>
                    <!-- Columna 1 -->
			<td>Nombre del estudiante :</td>
			<!-- Columna 2 -->
                        <td><form:input path="estudiante" /></td>
			<!-- Columna 3 donde aparece mensaje de error si es que lo hay. Con cssClass="error" indicamos que usa el estilo error-->
                        <td><form:errors path="estudiante" cssClass="error" /></td>
		</tr>
                <tr>
                    <td>Semestre :</td>
			<td><form:select path="semestre">
				   <form:option value="" label="Selecciona" />
				   <form:option value="1" label="Primero" />
				   <form:option value="2" label="Segundo" />
				   <form:option value="3" label="Tercero" />
                                   <form:option value="4" label="Cuarto" />
				   <form:option value="5" label="Quinto" />
				   <form:option value="6" label="Sexto" />
                                   <form:option value="7" label="Septimo" />
				   <form:option value="8" label="Octavo" />
			    </form:select></td>
			<td><form:errors path="semestre" cssClass="error" /></td>
		</tr>
                <tr>
			<td>�Cu�ndo desea iniciar clases? :</td>
                        <td><input type="text" id="datepicker" path="materia_1"><td>
			<td><form:errors path="materia_1" cssClass="error" /></td>
		</tr>
                
		<tr>
			<td>Materia 1 :</td>
                        <td>
                            <!--<div class="ct url">-->
                            <form:input path="materia_1" /></td>
                            <!--</div>-->
                        <td><form:errors path="materia_1" cssClass="error" /></td>
		</tr>

                <tr>
			<td>Materia 2 :</td>
			<td><form:input path="materia_2" /></td>
			<td><form:errors path="materia_2" cssClass="error" /></td>
		</tr>
                
                
                <tr>
			<td>Materia 3 :</td>
			<td><form:input path="materia_3" /></td>
			<td><form:errors path="materia_3" cssClass="error" /></td>
		</tr>
                
                <tr>
			<td>Taller :</td>
			<td>  
                            <div class="ui-widget">
                                <label for="tags"></label>
                            
                                <form:input id="tags" path="taller" />
                            </div></td>
			<td><form:errors path="taller" cssClass="error" /></td>
		</tr>

                <tr>
			<td>Idioma :</td>
			<td><form:radiobutton path="idioma" value="Ingles" label="Ingles" /> 
                            <form:radiobutton path="idioma" value="Aleman" label="Aleman" />
                            <form:radiobutton path="idioma" value="Frances" label="Frances" />
			</td>
			<td><form:errors path="idioma" cssClass="error" /></td>
		</tr>
                

		<tr>
		  <td colspan="3">
		    <br>
		  </td>
		</tr>
		<tr>		 
			<td colspan="3">
			  <center>
			    <input type="submit" value="Register">
			  </center>
			</td>		 
		</tr>
	</table>
	<form:errors path="*" cssClass="error" />
</form:form>
    


</body>
</html>