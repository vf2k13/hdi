<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Destalles Inscripcion</title>
</head>
<body>
  Detalles de inscripcion
  <hr>
  <!<!-- accedemos a los datos almacenados en el @SessionAttributes("inscripcion") del controlador -->
  Nombre   	: ${inscripcion.estudiante} <br/>
  Semestre        	    : ${inscripcion.semestre} <br/>  
  Materia 1      		: ${inscripcion.materia_1} <br/>
  Materia 2      		: ${inscripcion.materia_2} <br/>
  Materia 3      		: ${inscripcion.materia_3} <br/>
  Taller     			: ${inscripcion.taller} <br/>
  Idioma   		: ${inscripcion.idioma} <br/>

</body>
</html>