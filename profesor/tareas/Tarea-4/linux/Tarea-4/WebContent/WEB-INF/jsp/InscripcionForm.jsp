<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>Pagina de Registro</title>
   
   <!-- Especificación del estilo para los errores -->
   <style>
     .error {
        color: #ff0000;
        font-style: italic;
      }
   </style>
   
</head>
<body>

<form:form method="POST" commandName="inscripcion">
	<table border="0" bgcolor="11BBB0">	
		<tr>		 
			<td colspan="3">
			  <center>
			    <h1>Formulario de Inscripción escolar al semestre 2022-1</h1>
			  </center>
			</td>		 
		</tr>
		<tr>
                    <!-- Columna 1 -->
			<td>Nombre del estudiante :</td>
			<!-- Columna 2 -->
                        <td><form:input path="estudiante" /></td>
			<!-- Columna 3 donde aparece mensaje de error si es que lo hay. Con cssClass="error" indicamos que usa el estilo error-->
                        <td><form:errors path="estudiante" cssClass="error" /></td>
		</tr>
                <tr>
                    <td>Semestre :</td>
			<td><form:select path="semestre">
				   <form:option value="" label="Selecciona" />
				   <form:option value="1" label="Primero" />
				   <form:option value="2" label="Segundo" />
				   <form:option value="3" label="Tercero" />
                                   <form:option value="4" label="Cuarto" />
				   <form:option value="5" label="Quinto" />
				   <form:option value="6" label="Sexto" />
                                   <form:option value="7" label="Septimo" />
				   <form:option value="8" label="Octavo" />
			    </form:select></td>
			<td><form:errors path="semestre" cssClass="error" /></td>
		</tr>
		<tr>
			<td>Materia 1 :</td>
			<td><form:input path="materia_1" /></td>
			<td><form:errors path="materia_1" cssClass="error" /></td>
		</tr>
                <tr>
			<td>Materia 2 :</td>
			<td><form:input path="materia_2" /></td>
			<td><form:errors path="materia_2" cssClass="error" /></td>
		</tr>
                <tr>
			<td>Materia 3 :</td>
			<td><form:input path="materia_3" /></td>
			<td><form:errors path="materia_3" cssClass="error" /></td>
		</tr>
                <tr>
			<td>Talleres :</td>
			<td><form:radiobutton path="taller" value="Dibujo artistico" label="Dibujo artistico" /> 
                            <form:radiobutton path="taller" value="Programacion competitiva" label="Programacion competitiva" />
                            <form:radiobutton path="taller" value="Baile contemporaneo" label="Baile contemporaneo" />
			</td>
			<td><form:errors path="taller" cssClass="error" /></td>	
		</tr>
                <tr>
			<td>Idiomas :</td>
			<td><form:radiobutton path="idioma" value="Ingles" label="Ingles" /> 
                            <form:radiobutton path="idioma" value="Aleman" label="Aleman" />
                            <form:radiobutton path="idioma" value="Frances" label="Frances" />
			</td>
			<td><form:errors path="idioma" cssClass="error" /></td>
		</tr>
                

		<tr>
		  <td colspan="3">
		    <br>
		  </td>
		</tr>
		<tr>		 
			<td colspan="3">
			  <center>
			    <input type="submit" value="Register">
			  </center>
			</td>		 
		</tr>
	</table>
	<form:errors path="*" cssClass="error" />
</form:form>

</body>
</html>