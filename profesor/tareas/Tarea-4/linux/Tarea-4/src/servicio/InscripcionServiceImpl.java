package servicio;

import baseDeDatos.InscripcionesJDBCTemplate;
import dominio.Inscripcion;

/**
 * Clase que implementa la interfaz InscripcionService encargada de
 agregar un usuario a la base de datos
 * @author vic2k110
 */
public class InscripcionServiceImpl implements InscripcionService {

	@Override

	public void add( Inscripcion inscripcion ) {

		InscripcionesJDBCTemplate conn = new InscripcionesJDBCTemplate();
		try {
			  conn.insertarInscripcion( inscripcion );
		  } catch(Exception e)
		{
			System.out.println("Error NamingException: " + e.toString());
		}
	}
}
