package servicio;

import dominio.Inscripcion;
import dominio.User;
//import org.springframework.jdbc.core.RowMapper;

/**
 * Interfaz que especifica el método para insertar un usuario a la base de datos
 * @author vic2k110
 */
public interface InscripcionService {

	public void add( Inscripcion inscripcion );
}
