package control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InscripcionSuccessController {

	@RequestMapping("/inscripcionSuccess.htm")
	public String redirect()
	{
		return "inscripcionSuccess";
	}
}

