### ¿Cómo correr el contenedor para la base de datos?
Necesitamos tener instalado docker y docker compose. Para docker compose sólo hay que descargar unos archivos y moverlos a ua carpeta en específico Tutorial: https://docs.docker.com/compose/install/

Ejecutar docker-compose-yml con el siguiente comando en shell
```sh
docker-compose up
```

Si no queremos ver los logs y que se nos libere la terminal ponemos 
```sh
docker-compose up -d
```

### ¿Cómo conectarse al contenedor desde línea de comandos?
Listar los contenedores
```sh
docker container ls -a
```

Conectarse al contenedor de acuerdo a su PID
```sh
docker exec -ti [PID DEL CONTENEDOR] /bin/bash
```

### ¿Cómo hacer la conexión de Workbrench a la base de datos y correr el .sql?
1. Correr el contenedor con la base de datos
2. Abrir Workbrench
3. Ir a MySQL Connections y dar clic en el símbolo + para crear una nueva conexión
4. Poner cualquier nobre a la conexión
5. En Hostname poner localhost
6. En el puerto poner del contenedor, que en nuetro caso sería el 32132
7. Probar la conexión dando clic en Test Connection e ingresar la contraseña de la base de datos (especificada en el .yml)
8. dar clic en ok
9. Ir a file y abrir sql sript
10.  Seleccionar todas las líneas con ctr-a y dar clic en el rayito para ejecutar las sentencias del .sql
11.  Verificar en el apartado de de la base de datos que estén nuestras tablas. Adicionalmente se puede hacer esta comprobación entrando al contenedor y haciendo la consulta por línea de comandos.


### ¿Cómo operar sobre MySQL desde línea de comandos?
**Aquí se supone que se está dentro del contenedor**
Iniciar línea de comandos de MySQL
```sh
mysql -u root -p test
```
**Contraseña:** admin

Mostrar todas las bases de datos
```sh
show databases;
```

Usar una base de datos
```sh
use NombreBaseDatos;
```

Mostrar todas las tablas
```sh
show tables;
```

Ver todas las tuplas de una tabla
```sh
select * from nombreTabla;
```



### ¿Cómo importar este proyecto a netbeans?
- Borrar todo lo relacionado a archivos de configuración para netbeans


### Consideraciones importantes
- El proyecto no funciona sin el JDK 8

- La base de datos se crea en el .sql con el nombre test

- La contraseña de la base de datos es admin y el usuario es admin ó root

- Primero correr el contenedor de la base de datos para que la aplicación funcione, luego hacer la conexión con la base de datos y correr /FormularioDBSpring3MVC/BDFormulario.sql para tener la base de datos completamente lista con las tablas

- La base de datos no tiene datos

- Se modificó el puerto del archivo Formulario/DBSpring3MVC/src/servicio.properties por el 32132 que es el que tiene mapeado el contenedor de la base de datos.

- De momento no se puede escribir en la tabla usuarios porque hay una pequeña falla con el nombre de la tabla usuario, parace ser que se hace un query usando U en lugar de u para esta tabla



