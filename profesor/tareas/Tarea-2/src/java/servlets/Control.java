/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

/**
 *
 * @author GustavoMarquez
 */

import java.io.* ; 
import javax.servlet.* ;
import javax.servlet.http.* ;
import java.util.* ;

public class Control extends HttpServlet {
  
public void doGet( HttpServletRequest request, HttpServletResponse response )
      throws ServletException, IOException 
   {
    response.setContentType( "text/html" ) ;
    PrintWriter out = response.getWriter() ;
    String title = "Devolviendo datos de inscripción escolar" ;
    String DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">" ;

    out.println( DOCTYPE + "\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
                "<TABLE BORDER=1 ALIGN=CENTER>\n" +
                "<TR BGCOLOR=\"#FFAD00\">\n" +
                "<TH>Dato(s) del campo<TH>Dato(s) del Campo");
    
    Enumeration paramNames = request.getParameterNames();
    while( paramNames.hasMoreElements() ) 
      {

        String paramName = (String)paramNames.nextElement();
        out.println( "<TR><TD>" + paramName + "\n<TD>" );
        String[] paramValues = request.getParameterValues( paramName );
        if ( paramValues.length == 1 ) 
          {

           String paramValue = paramValues[ 0 ];
           if ( paramValue.length() == 0 )
              out.print( "<I>Ningun Valor</I>" );
           else
             out.print( paramValue );
          } 
        else 
          {
            out.println( "<UL>" );
            for( int i = 0; i < paramValues.length; i++ ) {
            out.println( "<LI>" + paramValues[ i ] );
              }
        out.println( "</UL>" );
      }
    }
    out.println( "</TABLE>\n</BODY></HTML>" );
  }

  public void doPost( HttpServletRequest request, HttpServletResponse response )
      throws ServletException, IOException 
       {
         doGet( request, response );
       }
}
