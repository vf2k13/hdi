/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.* ; 
import java.util.ArrayList;
import javabeans.Inscripcion;
import javax.servlet.* ;
import javax.servlet.http.* ;
import modelo.Operaciones;
import java.util.* ;


public class Control extends HttpServlet {
  //Variable global que se mantiene viva durante toda la ejecución para tener los datos de inscripción
  private static Inscripcion ins = null;

  public void service( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
    //Obtenemos el tipo de operación realizada
    String op = request.getParameter( "operacion" );//devuelve null si no se seleccionó ninguna operación
    
    
    //preguntamos si la cadena recibida es la operación inscribirse
    if ( op.equals( "inscribirse" ) ){
        //Guardamos en un bean los datos de incripcion
        ins = new Inscripcion(request.getParameter("Nombre"), request.getParameter("Materia 1"), request.getParameter("Materia 2"), request.getParameter("Materia 3"));
        System.out.println(request.getParameter("Materia 1"));

        //pasar el bean a la vista de confirmación para que el cliente confirme los datos de inscripción
        request.setAttribute("inscripciones", ins);
        RequestDispatcher rd = request.getRequestDispatcher( "/confirmacion.jsp" );
        rd.forward( request, response );
    }
    
    if ( op.equals( "confirmar" ) ){  
        //Instanciamos a opera para hacer la inserción en la base de datos
        Operaciones oper = new Operaciones( request );    
        oper.grabaInscripcion(ins);
        
        RequestDispatcher rd = request.getRequestDispatcher( "/exito.html" );
        rd.forward( request, response );
        
    }

    }
}
