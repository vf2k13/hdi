/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javabeans;

/**
 * Clase que modela una clase
 * @author Fidel R
 */
public class Inscripcion {
    private String estudiante;
    private String materia_1;
    private String materia_2;
    private String materia_3;

    public Inscripcion(String estudiante, String materia_1, String materia_2, String materia_3) {
        this.estudiante = estudiante;
        this.materia_1 = materia_1;
        this.materia_2 = materia_2;
        this.materia_3 = materia_3;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    public String getMateria_1() {
        return materia_1;
    }

    public void setMateria_1(String materia_1) {
        this.materia_1 = materia_1;
    }

    public String getMateria_2() {
        return materia_2;
    }

    public void setMateria_2(String materia_2) {
        this.materia_2 = materia_2;
    }

    public String getMateria_3() {
        return materia_3;
    }

    public void setMateria_3(String materia_3) {
        this.materia_3 = materia_3;
    }
    
}
