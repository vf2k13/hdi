/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.sql.*;
import javabeans.*;
import java.util.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Clase para hacer operaciones sobre la base de datos en access.
 * @author Fidel R
 */
public class Operaciones {
    
    HttpServletRequest request;
    
   public Operaciones( HttpServletRequest request )  {
       
       this.request = request;
   }
    
  // Método común para la obtención
  // de conexiones
  public Connection getConnection() {
    Connection cn = null;
    try {
//      Antes de Java 8: 
//      Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" );
//      cn = DriverManager.getConnection( "jdbc:odbc:mensajes" );

//      A partir de Java 8:
//      Ya no se requiere registrar la BD en odbcad32.exe located in C:\Windows\WOW64 
        Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");

//        cn = DriverManager.getConnection("jdbc:ucanaccess://C:/Temp/mensajes.mdb");
         ServletContext ctx = request.getServletContext();
         //especificamos el nombre del archivo donde está la base de datos
         String urldb = ctx.getRealPath( "mensajes" + ".mdb" ); 
         cn = DriverManager.getConnection("jdbc:ucanaccess://" + urldb );   
    }
    catch( Exception e ) { e.printStackTrace(); }
    return cn;
  }
  
  public ArrayList obtenerInscripciones( String nombreEstudiante ) {
    Connection cn = null;
    ArrayList inscripciones = null;
    Statement st;
    ResultSet rs;
    try {
      cn = getConnection();
      st = cn.createStatement();
      String tsql;
      //Especificamos la consulta de acuerdo a los campos de la base de datos
      tsql = "select * from mensajes where estudiante = '" + nombreEstudiante + "'";
      //Ejecutamos la consulta y el resultado se almacena en rs 
      rs = st.executeQuery( tsql );
      inscripciones = new ArrayList(); //arreglo para guardar los beans
      // Para cada inscipcion encontrada crea un objeto
      // Inscripcion y lo añade a la colección ArrayList
      while( rs.next() ) { //recorremos cada elemento obtenido como resultado de la consulta
        Inscripcion ins = new Inscripcion( rs.getString("estudiante"), rs.getString("materia_1"), rs.getString("materia_2"), rs.getString("materia_3"));
        inscripciones.add( ins ); //agregamos el bean al arreglo resultante
      }
      cn.close(); //cerramos la conexión con la bse de datos
    }
    catch( Exception e ){ e.printStackTrace(); }
    return( inscripciones );
  }

  public void grabaInscripcion( Inscripcion ins ) {
    Connection cn;
    Statement st;
    ResultSet rs;
    try {
      cn = getConnection();
      st = cn.createStatement();
      String tsql;
      // A partir de los datos del mensaje construye
      // la cadena SQL para realizar su inserión
      tsql = "Insert into mensajes values( '";
      tsql += ins.getEstudiante()+ "','" + ins.getMateria_1() + "','" + ins.getMateria_2() + "','" + ins.getMateria_3() + "')";
      
      st.execute( tsql );
      cn.close();
    }
    catch( Exception e ) { e.printStackTrace(); }
  }
    
}
