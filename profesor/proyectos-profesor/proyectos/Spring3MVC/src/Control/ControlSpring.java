package Control;

import org.springframework.stereotype.Controller; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.servlet.ModelAndView;   

@Controller  //Indicamos que esta calse es un controlador

//Controlador
public class ControlSpring {
	
	// Esto se va a a traducir a un servlet y se va a montar en Tomcat
	//Se va a ejecutar un macro y se va a hacer todo en autom�tico todo
	// lo hecho en proyectos anteriores. Este m�todo decribe la acci�n a realizar
	// cuando se recibe la petici�n /hello. Como cuando revizabamos que acci�n �
	// petici�n se solicit� con un if-else
	/**
	 * M�todo que reponde a la petici�n /hello
	 * @return una vista
	 */
	@RequestMapping("/hello") //Petici�n /hello
	public ModelAndView helloWorld() { 
		//Acci�n a realizar de acuerdo a la petici�n /hello
		String message = "Hello World, Spring 3.0!";   
		//Pasamos como parametros
		//	"hello" = presentamos la vista hello, la de JSP y le pasamos la
		//		variable "mensaje" como parametro al JSP
		//"message" = variable que va a guardar informaci�n
		// message = lo que va a guardar la varaible anterior 
		return new ModelAndView("hello", "message", message); //Devolvemos el JSP con message
		} 
}
