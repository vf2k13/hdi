<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>Spring 3.0 MVC Hola a Todos</title> <!-- T�tulo de la ventana -->
</head>

<!--Archivo de bienvenida -->
<body>
	<!-- Vamos a referenciar a hello.html mostrando el texto Say Hello. Va a
	 aparecer un v�nvulo a hello.html con el texto Say Hello.
	 El control recibe la peptici�n hello.html, como qued� especificado en web.xml-->
   <a href="hello.html">Say Hello</a> 
</body>
</html>