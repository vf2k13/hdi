<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>Spring 3.0 MVC Uso de Formas</title>
</head>

<body>
   <!-- Llamamos al control con la petici�n contactos.html, al hacer el
   	llamado, se hace la busqueda en la carpeta Controlador de del request
   	Mapping que corresponda a contactos.html, que ser�a en este
   	caso /contactos en ControlForma.java -->
   <jsp:forward page="contactos.html"></jsp:forward>
</body>

</html>