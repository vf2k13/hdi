<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<!-- Vista que se ve al inicio -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Spring 3 MVC Agenda Personal</title>
</head>
<body>
<h2>Agenda Personal</h2>
<!-- al llenar los datos y dar clic en submit se llama al controlador 
	correspondiente a add.Contact que es /addContact en la clase
	controlForma.java -->
<form:form method="post" action="addContact.html">
	<!--  Definimos una tabla y al se llamada esta vista se va a tener una
	 instancia de Contacto, por lo cual los campos a recibir van a ser
	 pasados al bean, esto lo hace spring al momento de hacer submit.
	 Cuando se Da clic en submit, spring busca en la carpeta de Control
	 el controlador con el m�todo correspondiente, en es te 
	 caso es /addContact que est� en ControlForma.java y se le pasa al
	 m�todo la instancia Contacto con sus atributos inicializados con
	 los que se le pasaron a este formulario-->
	<table>
	<tr>
		<td><form:label path="firstname">First Name</form:label></td>
		<td><form:input path="firstname" /></td> 
	</tr>
	<tr>
		<td><form:label path="lastname">Last Name</form:label></td>
		<td><form:input path="lastname" /></td>
	</tr>
	<tr>
		<td><form:label path="email">Email</form:label></td>
		<td><form:input path="email" /></td>
	</tr>
	<tr>
		<td><form:label path="telephone">Telephone</form:label></td>
		<td><form:input path="telephone" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="Add Contact"/>
		</td>
	</tr>
</table>		
</form:form>
</body>
</html>