package Control;

import Formas.Contacto;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
//Esta clase es un controlador, por eso el @Controller
@Controller

public class ControlForma {
	/**
	 * M�todo que da respuesta a la petici�n /addContact y que espera
	 * que le pasen un contacto ya instanciado en contacto.jsp
	 * @param contacto Un contacto instanciado con los datos que envi� el usuario
	 * @param result Para manejar errores, de momento no se usa
	 * @return Una cadena como instrucci�n
	 */
	@RequestMapping( value = "/addContact", method = RequestMethod.POST )//Especificamos el tipo de petici�n a la cual dar respuesta, en este caso es una petici�n POST
	public String addContact( @ModelAttribute( "contacto") Contacto contacto, BindingResult result) {
		
		System.out.println( "Nombre: " + contacto.getFirstname() );
		System.out.println( "Apellido: " + contacto.getLastname() );	
		System.out.println( "Mail: " + contacto.getEmail() );	
		System.out.println( "Tel�fono: " + contacto.getTelephone() );	
		
		//Redireccionamos a contactos.html para caer en un ciclo que mantiene
		//agregando contactos
		return "redirect:contactos.html";		
	}	
	
	/**
	 * M�todo que responde a una petici�n contactos.html, por eso en 
	 * RequestMapping tenemos /contactos. En el cuerpo de este m�todo
	 * se especifica lo que se debe hacer cuando se recibe una petici�n
	 * de contactos.html
	 * @return un model and view
	 */
	@RequestMapping( "/contactos" )
	public ModelAndView showContacts() {
		//Acci�n a llevar a cabo
		return new ModelAndView( "contacto", "command", new Contacto() );
		//En la l�nea anterior se devuelve un modelAndView tal que
		//"contacto" = Llamada de la vista contacto.jsp con el parametro command
		//"command" = Para almacenar aqu� una instancia de tipo Contacto
		//new Contacto() = Creamos unbean de tipo Contacto
	}
}
