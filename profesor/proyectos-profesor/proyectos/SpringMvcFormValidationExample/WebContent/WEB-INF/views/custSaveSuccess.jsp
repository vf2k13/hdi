<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page session="false" %>
<html>
<head>
	<title>Customer Saved Successfully</title>
</head>
<body>
<h3>
	Customer Saved Successfully.
</h3>

<strong> - Customer Name: ${customer.name}</strong><br>
<strong> - Nombre Completo: ${customer.nombrecompleto}</strong><br>
<strong> - C&oacute;digo Postal: ${customer.codigopostal}</strong><br>
<strong> - Sueldo: ${customer.sueldo}</strong><br> 
<strong> - Precio: ${customer.precio}</strong><br> 
<strong> - Celular: ${customer.celular}</strong><br> 
<strong> - Color de ojos: ${customer.colorojos}</strong><br>
<strong> - Tarjeta de Casa de Bolsa: ${customer.tarjetacasabolsa}</strong><br>
<strong> - RFC: ${customer.rfc}</strong><br>
<strong> - Customer Email: ${customer.email}</strong><br>
<strong> - Customer Age: ${customer.age}</strong><br>
<strong> - Customer Gender: ${customer.gender}</strong><br>
<strong> - Customer Birthday: <fmt:formatDate value="${customer.birthday}" type="date" /></strong><br>
<strong> - Customer Phone: ${customer.phone}</strong><br>

</body>
</html>
