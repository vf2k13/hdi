<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome</title>
</head>
<body>
	<div align="center">
		<h2>Welcome ${userForm.email}!</h2>
		<h3>You have logged in successfully.</h3> 		
		<br>
		<!-- Hacemos una referencia a la petición ingresar en CustomerController.java haciendo una petición GET -->
		<a href="ingresar">Ingresar</a> 
	</div>
</body>
</html>