<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Customer Save Page</title>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
</head>
<body>

	<springForm:form method="POST" commandName="customer"
		action="saveCustomer">
		<H3>Formato de Inscripción</H3>
		<table>
			<tr>
				<td>Name:</td>
				<td><springForm:input path="name" /></td>
				<td><springForm:errors path="name" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Nombre completo:</td>
				<td><springForm:input path="nombrecompleto" /></td>
				<td><springForm:errors path="nombrecompleto" cssClass="error" /></td>
			</tr>
			<tr>
				<td>C&oacute;digo Postal:</td>
				<td><springForm:input path="codigopostal" /></td>
				<td><springForm:errors path="codigopostal" cssClass="error" /></td>
			</tr>	
			<tr>																															
  				<td>Sueldo:</td>
				<td><springForm:input path="sueldo" /></td>
				<td><springForm:errors path="sueldo" cssClass="error" /></td>
			</tr> 																																
			<tr>																															
  				<td>Precio (Opcional):</td>
				<td><springForm:input path="precio" /></td>
				<td><springForm:errors path="precio" cssClass="error" /></td>
			</tr>
			<tr>																															
  				<td>Celular:</td>
				<td><springForm:input path="celular" /></td>
				<td><springForm:errors path="celular" cssClass="error" /></td>
			</tr>	
			<tr>																															
  				<td>Color de Ojos:</td>
				<td><springForm:input path="colorojos" /></td>
				<td><springForm:errors path="colorojos" cssClass="error" /></td>
			</tr>		
			<tr>																															
  				<td>Tarjeta Casa de Bolsa:</td>
				<td><springForm:input path="tarjetacasabolsa" /></td>
				<td><springForm:errors path="tarjetacasabolsa" cssClass="error" /></td>
			</tr>	
			<tr>																															
  				<td>RFC con homoclave:</td>
				<td><springForm:input path="rfc" /></td>
				<td><springForm:errors path="rfc" cssClass="error" /></td>
			</tr>																											 																																			
			<tr>
				<td>Mail:</td>
				<td><springForm:input path="email" /></td>
				<td><springForm:errors path="email" cssClass="error" /></td>
			</tr>	
			<tr>
				<td>Age:</td>
				<td><springForm:input path="age" /></td>
				<td><springForm:errors path="age" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Gender:</td>
				<td><springForm:select path="gender">
						<springForm:option value="" label="Select Gender" />
						<springForm:option value="MALE" label="Male" />
						<springForm:option value="FEMALE" label="Female" />
					</springForm:select></td>
				<td><springForm:errors path="gender" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Birthday:</td>
				<td><springForm:input path="birthday" placeholder="MM/dd/yyyy"/></td>
				<td><springForm:errors path="birthday" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Phone:</td>
				<td><springForm:input path="phone" /></td>
				<td><springForm:errors path="phone" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="Save Customer"></td>
			</tr>
		</table>

	</springForm:form>

</body>
</html>