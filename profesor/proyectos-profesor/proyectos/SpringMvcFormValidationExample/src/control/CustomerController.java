package control;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

//Biblioteca de logger proporcionado por Apache
//	para registrar los logs
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import utils.ContextoServlet;

import modelo.Customer;

@Controller

public class CustomerController { 

	//Al principio se debe poner la creaci�n del logger paara que se lleve registro de la clase
	private static final Logger logger = 
	         Logger.getLogger( CustomerController.class );

	private Map<String, Customer> customers = null;
 	ContextoServlet contextoServlet;

 	//Recibimos un ContextoServlet que nos dice el contexto en que se ejecuta el server
 	//	as� como las rutas de ejecuci�n

	@Autowired	
	public CustomerController( ContextoServlet contextoServlet ) {
		
		this.contextoServlet = contextoServlet ;
		String pathlog4j = contextoServlet.getServletContext().getRealPath("/").substring(0, contextoServlet.getServletContext().getRealPath("/").indexOf(".metadata")) + contextoServlet.getServletContext().getServletContextName() + "\\WebContent\\WEB-INF\\log4j.xml" ;
		System.out.println( "Context log4j.xml: " + pathlog4j );
		DOMConfigurator.configure( pathlog4j );
		
		customers = new HashMap<String, Customer>();
//      El ServletContext evita hacer lo siguiente:		
//		DOMConfigurator.configure("C:\\Users\\Gustavo M�rquez\\workspace\\Spring\\SpringMvcFormValidationExample\\WebContent\\WEB-INF\\log4j.xml");
		logger.info( "------------------------------" );
		logger.info( "Inicia " + contextoServlet.getServletContext().getContextPath() );
		logger.info( "Constructor CustomerController" );
	}

	@RequestMapping( value = "ingresar", method = RequestMethod.GET )
	public String saveCustomerPage( Model model ) {

		logger.info( "Returning from LoginForm.jsp page" );
		//Creamos el bean y lo enviarmos al modelo para llenarle sus datos
		model.addAttribute( "customer", new Customer() );
		return "custSave";
	}

	@RequestMapping( value = "saveCustomer", method = RequestMethod.POST )
	public String saveCustomerAction( @Valid Customer customer,
			            BindingResult bindingResult, Model model ) {
		
		if ( bindingResult.hasErrors() ) {
			//Si hay errores, entonces mostramos el log de lo ocurrido
			logger.info( "Errors => sending custSave.jsp page" );
			//Devolvemos la misma vista con los errores ya cargados
			return "custSave";
		  }
		logger.info( "No error => sending custSaveSuccess.jsp page" );
		model.addAttribute( "customer", customer );
		customers.put( customer.getEmail(), customer );
		return "custSaveSuccess";
	}
}
