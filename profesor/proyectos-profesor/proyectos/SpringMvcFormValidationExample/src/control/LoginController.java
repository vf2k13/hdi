package control;

import java.util.Map;

import javax.validation.Valid;

import modelo.User;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
	
	@RequestMapping( value = "/login", method = RequestMethod.GET )
	public String viewLogin( Map<String, Object> model ) {
		
		//Creamos un bean
		User user = new User();
		//Le pasamos el bean al model
		model.put( "userForm", user );
		//Devolvemos la vista LoginForm
		return "LoginForm";
	}

	@RequestMapping( value = "/login", method = RequestMethod.POST )
	public String doLogin( @Valid @ModelAttribute( "userForm" ) User userForm,
			BindingResult result, Map<String, Object> model ) {

		if (result.hasErrors()) 
			return "LoginForm";	//Presentamos la misma vista pero cno los errores	
		return "LoginSuccess";
	}
}