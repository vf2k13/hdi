package modelo;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


public class User {
	//En messages.properties tenemos definido el mensaje NotEmpty.userForm.email
	@NotEmpty( message = "NotEmpty.userForm.email" )	
	@Email
	private String email;
	
	//Especificamos que queremos una contraseña de mínimo y máximo, también
	//	especificamos el mensaje de error a mostrar
	@NotEmpty( message = "Please enter your password." )
	@Size( min = 6, max = 15, message = "Your password must between 6 and 15 characters" )
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword( String password ) {
		this.password = password;
	}
}
