package modelo;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import validacion.Phone;

public class Customer {

	@Size(min=2, max=30) 
//  Con la siguiente al correr, envía error:
//    @Pattern(regexp = "{A-Za-z}+")
    @Pattern(regexp = "^[A-Za-z]+$")
    private String name;

    //Especificamos expresión regular para nombre 
    @Pattern(regexp = "([A-Z]{1}[a-z]{1,29} +){2,}([A-Z]{1}[a-z]{1,29})$")	
    private String nombrecompleto;
	
    @Pattern(regexp = "^[0-9]{1,5}$")
    private String codigopostal;
    
//    @Pattern(regexp = "^\\d+(\\.\\d\\d)?$")
      @Pattern(regexp = "\\d+(\\.\\d{1,2})?")
        
//    private float sueldo;
//    => envía error: No validator could be found for type: java.lang.Float: 
    private String sueldo;
    
    @Pattern(regexp = "( *|\\d+(\\.\\d{1,2})?)")      
    private String precio;

    @Pattern(regexp = "^(04455-)\\d{8}")      
    private String celular = "04455-";
    
    @Pattern(regexp = "^([Nn]egros?|[Aa]zul(es)?|[Vv]erdes?|[Cc]afes?)$")      
    private String colorojos ;
    
//  Las tarjetas de casa de bolsa contienen 15 dígitos e inician con 478 y terminan con 472    
    @Pattern(regexp = "^(478)\\d{9}(472)$")      
    private String tarjetacasabolsa;

    @Pattern(regexp = "^[A-Z]{4}[0-9]{6}[0-9][A-Z][0-9]$")      
    private String rfc;
    
    @NotEmpty @Email
    private String email;
     
    @NotNull @Min(18) @Max(100)
    private Integer age;
     
    @NotNull
    private Gender gender;
     
    @DateTimeFormat( pattern="dd/mm/yyyy" )
    @NotNull @Past
    private Date birthday;
    
    @Phone
    private String phone;
    
    public enum Gender {
		MALE, FEMALE
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getNombrecompleto() {
		return nombrecompleto;
	}

	public void setNombrecompleto( String nombrecompleto ) {
		this.nombrecompleto = nombrecompleto;
	}	

	public String getCodigopostal() {
		return codigopostal;
	}

	public void setCodigopostal( String codigopostal ) {
		this.codigopostal = codigopostal;
	}	
		
	public String getSueldo() {
		return sueldo;
	}

	public void setSueldo( String sueldo ) {
		this.sueldo = sueldo;
	}	

	public String getPrecio() {
		return precio;
	}

	public void setPrecio( String precio ) {
		this.precio = precio;
	}	
	
	public String getCelular() {
		return celular;
	}

	public void setCelular( String celular ) {
		this.celular = celular;
	}	
		
	public String getColorojos() {
		return colorojos;
	}

	public void setColorojos( String colorojos ) {
		this.colorojos = colorojos;
	}	
	
	public String getTarjetacasabolsa() {
		return tarjetacasabolsa;
	}

	public void setTarjetacasabolsa( String tarjetacasabolsa ) {
		this.tarjetacasabolsa = tarjetacasabolsa;
	}	
	
	public String getRfc() {
		return rfc;
	}

	public void setRfc( String rfc ) {
		this.rfc = rfc;
	}	
			
	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge( Integer age ) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender( Gender gender ) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday( Date birthday ) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone( String phone ) {
		this.phone = phone;
	}	
}
