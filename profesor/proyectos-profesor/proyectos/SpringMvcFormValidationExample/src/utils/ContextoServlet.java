package utils;

import javax.servlet.ServletContext;
import org.springframework.web.context.ServletContextAware;

public class ContextoServlet implements ServletContextAware {

	ServletContext context; 
	@Override
	public void setServletContext( ServletContext servletContext ) {
		
	   this.context = servletContext;
	   System.out.println( "Context Real Path: " + context.getRealPath( "/log4j.xml" ) );
	   System.out.println( "Context Path: " + context.getContextPath() );
	   System.out.println( "Context Name: " + context.getServletContextName() );		 		 
	}	
	
	public ServletContext getServletContext() {
		
	   return context;
	}
}
