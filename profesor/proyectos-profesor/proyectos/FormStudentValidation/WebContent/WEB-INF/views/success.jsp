<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Student Enrollment Detail Confirmation</title>
	<link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
</head>
<body>
	<div class="success">
		Confirmation message : ${success}
		<br>
		<br>
		We have also sent you a confirmation mail to your email address : ${student.email}.
	</div>
	<br>	
	<div class="success">
	  <br>                        
        <ul class="list-group-item">
          <li class="list-group-item">
             Nombre: ${student.firstName}.
          </li>
          <li class="list-group-item">
             Apellido: ${student.lastName}.
          </li>
          <c:choose>
             <c:when test="${student.sex eq 'M'}">
               <li class="list-group-item">
                  Sexo: Masculino.
               </li>   
             </c:when>
             <c:when test="${student.sex eq 'F'}">
               <li class="list-group-item">
                  Sexo: Femenino.
               </li>   
            </c:when>
         </c:choose>                       
         <li class="list-group-item">
            Fecha de Nacimiento: ${student.dob}.
         </li>
         <li class="list-group-item">
            Mail: ${student.email}.
         </li>
         <li class="list-group-item">
            Nivel: ${student.section}.
         </li>
         <li class="list-group-item">
            Pa�s: ${student.country}.
         </li>
         <li class="list-group-item">
            Primer intento: ${student.firstAttempt}.
         </li>
         <li class="list-group-item">
            �rea: ${student.subjects}.
         </li>                                                      
        </ul>        
		<br>
	</div>	 
</body>
</html>