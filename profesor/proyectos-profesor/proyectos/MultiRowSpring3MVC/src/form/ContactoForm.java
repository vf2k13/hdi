package form;

import java.util.List;

public class ContactoForm {

	private List<Contacto> contactos;

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos( List<Contacto> contactos ) {
		this.contactos = contactos;
	}
}
