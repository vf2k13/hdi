package control;

import java.util.ArrayList;
import java.util.List;

import form.Contacto;
import form.ContactoForm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContactoControl {

	private static List<Contacto> contactos = new ArrayList<Contacto>();

	static {
		contactos.add(new Contacto("Garc�a", "Alejandro", "garcia.a@unam.mx.com", "2-37-15-00"));
		contactos.add(new Contacto("Dur�n", "Gabriela", "duran.g@unam.mx.com", "5-77-23-43"));
		contactos.add(new Contacto("Mart�nez", "Virginia", "mar.viky@unam.mx.com", "3-91-19-19"));
		contactos.add(new Contacto("Flores", "Daniel", "dany.flores@unam.mx.com", "4-56-78-92"));
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ModelAndView get() {
		
		ContactoForm contactoForm = new ContactoForm();
		contactoForm.setContactos(contactos);
		
		return new ModelAndView("add_contacto" , "contactoForm", contactoForm);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("contactoForm") ContactoForm contactoForm) {
		System.out.println(contactoForm);
		System.out.println(contactoForm.getContactos());
		List<Contacto> contactos = contactoForm.getContactos();
		
		if( null != contactos && contactos.size() > 0 ) {
			ContactoControl.contactos = contactos;
			for (Contacto contacto : contactos) {
				System.out.printf("%s \t %s \n", contacto.getFirstname(), contacto.getLastname());
			}
		}		
		return new ModelAndView("show_contacto", "contactoForm", contactoForm);
	}		
}
