<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Ejemplo Spring 3 MVC Multipe Row</title>
  </head>
<body>
  <h2>Lista de Contactos</h2>
  <table width="80%" border="1" bgcolor="FFFF00">
	<tr bgcolor="FFDD01">
		<th>Name</th>
		<th>Lastname</th>
		<th>Email</th>
		<th>Phone</th>
	</tr>
	<c:forEach items="${contactoForm.contactos}" var="contacto" varStatus="status">
		<tr>
			<td>${contacto.firstname}</td>
			<td>${contacto.lastname}</td>
			<td>${contacto.email}</td>
			<td>${contacto.phone}</td>
		</tr>
	</c:forEach>
  </table>	
  <br/>
  <input type="button" value="Back" onclick="javascript:history.back()"/>
</body>
</html>