<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Ejemplo Spring 3 MVC</title>
  </head>
<body>
<h2>Ejemplo Spring MVC Multiple Row Form</h2>
<form:form method="post" action="save.html" modelAttribute="contactoForm">
	<table border="1" bgcolor="FFFF00">
	<tr bgcolor="FFDD01">
		<th>No.</th>
		<th>Name</th>
		<th>Lastname</th>
		<th>Email</th>
		<th>Phone</th>
	</tr>
	<c:forEach items="${contactoForm.contactos}" var="contacto" varStatus="status">
		<tr>
			<td align="center">${status.count}</td>
			<!-- Accedemos ${status.index} es para acceder al bean y value=${contacto.atributo es para recibir un input y cargar esta información al bean} -->
			<td><input name="contactos[${status.index}].firstname" value="${contacto.firstname}"/></td>
			<td><input name="contactos[${status.index}].lastname" value="${contacto.lastname}"/></td>
			<td><input name="contactos[${status.index}].email" value="${contacto.email}"/></td>
			<td><input name="contactos[${status.index}].phone" value="${contacto.phone}"/></td>
		</tr>
	</c:forEach>
</table>	
<br/>
<input type="submit" value="Save" />	
</form:form>
</body>
</html>