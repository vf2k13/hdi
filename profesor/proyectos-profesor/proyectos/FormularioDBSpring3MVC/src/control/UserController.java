package control;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import baseDeDatos.UsuariosJDBCTemplate;
import dominio.User;
import servicio.UserService;
import validator.UserValidator;


@Controller //indicamos que es un controlador
@RequestMapping( "/userRegistration.htm" ) //Petición a la que responden los métodos
@SessionAttributes("user") //Duante la sesión se va a mantener el atributo user, no queremos que se pierda esta inf. durante la sesión

public class UserController {

	private UserService userService;
	private UserValidator userValidator;
 	
 	//Indicamos la inyección de dependencias, se mandan a llamar las instancias especificadas en dispatcher-servlet
	@Autowired
	public UserController( UserService userService, UserValidator userValidator ) {
		//Se reciben dos instancias sacadas del dispatcher-servlet.xml
		// Estas instancias ya estan listos para usarse, sólo necesitamos
		//  referenciarlos a los atributos de esta clase
		this.userService = userService;
		this.userValidator = userValidator;
	}
	 
	//Respuestas a peticiones GET
	@RequestMapping( method = RequestMethod.GET )
	public String showUserForm( ModelMap model, HttpServletRequest request ) {
		//Model nos sirve para pasarle pámetros a las vistas, así que
		//	instanciamos un User y lo en viamos a la vista por medio del model
		User user = new User();
		model.addAttribute( "user", user);//model tiene una colección de objetos
		return "userForm"; //Vista a devolver al cliente
	}

	//Respuestas a peticiones OST
	@RequestMapping( method = RequestMethod.POST ) //Respuesta a POSt
	public String onSubmit( @ModelAttribute("user") User user, BindingResult result , HttpServletRequest request ) 
	 {
	 	//Tomamos el user del formulario (lo tiene el model) y lo
	 	//	recibimos como parámetro
		int MaxIntentos = 6;//intentos máximos para tratar de registrarse

		//Usamos el userValidator para validar el usuario y le pasamos
		//	el result para la parte de errores
		userValidator.validate( user, result );
		if ( result.hasErrors() ) 
			return "userForm";//caso en que hubo errores, entonces devolvemos la forma
	   else {
			
			String formulario = "";
			final String userIpAddress = request.getRemoteAddr();
	        System.out.println( "User IpAddress: " + userIpAddress );
	        UsuariosJDBCTemplate reg = new UsuariosJDBCTemplate();
	        reg.addIntento( userIpAddress );
	        if ( reg.getIntentos(userIpAddress) >= MaxIntentos )
	            formulario = "Exceso";	          
	        else
	          {
	        	userService.add( user );
	        	formulario = "redirect:userSuccess.htm";
	          }			
			return formulario;
		 }
	}
}

