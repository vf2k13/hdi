package control;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import form.FormContacto;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class ControlSpring {

	//Por ser est�tico, se mantiene durante la ejecuci�n contactMap
	//
	private static Map<String, String> contactMap = new HashMap<String, String>();
	static {
		//La primera entrada es la llave y la segunda el valor
		contactMap.put( "Nombre:", "John" );
		contactMap.put( "Apellido:", "Lennon" );
		contactMap.put( "G�nero:", "Rock, Pop" );
	  }
	
	@RequestMapping( value = "/show", method = RequestMethod.GET )
	public ModelAndView get() {
		
		FormContacto contactForm = new FormContacto();
		//Le pasamos el hasMap al bean
		contactForm.setContactMap( contactMap );
		
		//Llamamos a la vista add_contact y le pasamos el bean formContacto
		return new ModelAndView( "add_contact" , "formContacto", contactForm );
	}
	
	@RequestMapping( value = "/add", method = RequestMethod.POST )
	public ModelAndView save( @ModelAttribute("Contactform") FormContacto contactForm ) {

		System.out.println( contactForm );	
		System.out.println( contactForm.getContactMap() );	
		System.out.println( contactForm.getContactMap().values().toString() );
		
		Map<String, String> contactMap = contactForm.getContactMap();
		Iterator<String> it = contactMap.keySet().iterator();
		while( it.hasNext() ) {
		  String key = (String)it.next();
		  System.out.println( "Clave: " + key + " -> Valor: " + contactMap.get(key) );
		 }	
		//Regresamos la vista de show_contact para mostrar los datos que nos 
		//	enviaron a manera de confirmaci�n de guardado.  			
		return new ModelAndView( "show_contact", "formContacto", contactForm );
	}	
}
