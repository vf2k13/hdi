<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Spring 3 MVC HashMap Form </title>
</head>
<body>
	<h2>Contacto Salvado !</h2>
	<table border="1" bgcolor="11BCDE">
		<tr bgcolor="FFDD01">
		    <th>N�mero</th>
			<th>Campo</th>
			<th>Valor</th>
			<th>Observaci�n</th>
		</tr>
		<c:forEach items="${formContacto.contactMap}" var="contactMap"
			varStatus="status">
			<tr>
			    <td><center><c:out value="${status.count}" /></center></td>
				<td>${contactMap.key}</td>
				<td>${contactMap.value}</td>							
                <c:choose>
                    <c:when test="${status.count == 1}"><td>Este es el primer registro</td></c:when>  
                    <c:when test="${status.count == 3}"><td>Este es el �ltimo registro</td></c:when>   
                    <c:otherwise><td>Este es un registro intermedio</td></c:otherwise>           
                </c:choose>	
                						
			</tr>
		</c:forEach>
	</table>
	<br />
<!-- 	<input type="button" value="Back" onclick="javascript:history.back()" /> -->
	<!--Bot�n que permite volver a la p�gina anterior y tambi�n muestra un alert-->
	<input type="button" value="Back" onclick="javascript:history.back(alert('Hasta la vista !!!'))" />
	
</body>
</html>