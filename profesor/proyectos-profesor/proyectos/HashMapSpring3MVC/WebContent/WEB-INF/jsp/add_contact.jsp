<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Spring 3 MVC HashMap Forma</title>
</head>
<body>
   <h2>Spring 3 MVC HashMap Form</h2>
   <!-- Los datos se enviar�n al add en el controlador-->
   <form:form method="post" action="add.html" modelAttribute="Contactform">
	  <table border="1" bgcolor="11BCDE">
  	     <tr bgcolor="FFDD01">
  	      <th>N�mero</th>
  	      <th>�ndice</th>
		  <th>Campo</th>
		  <th>Valor</th>
		  <th>Observaci�n</th>
	     </tr>
	     <!-- Iteramos sobre cada elemento del hashmap
	     	var va a ser la variable del for each, var va a ser cada rengl�n
	     	de la tabla. y varStatus va a ser el atributo del rengl�n var-->
	     <c:forEach items="${formContacto.contactMap}" var="contactMap" varStatus="status">
		   <tr>
	     	<!--status.count es el contador de la tabla-->
		     <td><center><c:out value="${status.count}" /></center></td>
		    <!--status.index es el �ndice que usa el elemento en el rengl�n-->
		     <td><center><c:out value="${status.index}" /></center></td>
		     <!--LLave del elemento-->
			 <td>${contactMap.key}</td>
			 <td><input name="contactMap['${contactMap.key}']" value="${contactMap.value}"/></td>
			 <c:if test="${status.first}"><td>Este es el primer registro</td></c:if> 
             <c:if test="${status.last}"><td>Este es el �ltimo registro</td></c:if> 			 			 
		   </tr>
	     </c:forEach>
      </table>	
      <br/>

      <input type="submit" value="Save" />
	
   </form:form>

</body>
</html>