package spring.control;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import spring.modelo.*;
import spring.bean.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Control {

	private static DummyDB dummyDB = new DummyDB();

	@RequestMapping( value = "/get", 
			         method = RequestMethod.GET )
	public ModelAndView index() {

		User userForm = new User();
		return new ModelAndView( "user", "userForm", userForm );
	}

	//Método que responde con la lista de las sugerencias convertida en
	//	una cadena. Con @RequestParam("term") consultamos lo que tecleó el
	//	usuario y lo pasamos como quey
	@RequestMapping( value = "/get_country_list", 
					 method = RequestMethod.GET )
	public @ResponseBody String getCountryList( @RequestParam("term") String query ) {
		
		System.out.println( "Query country: " + query );
		//Hacemos el query y lo almacenamos en una lista de cadenas
		List<String> countryList = dummyDB.getCountryList( query );
        System.out.println( countryList.toString() );
        //Devolvemos la lista convertida en una ccadena
		return countryList.toString();
	}

	@RequestMapping( value = "/get_tech_list", 
					 method = RequestMethod.GET )
	public @ResponseBody String getTechList( @RequestParam("term") String query ) {
		System.out.println( "Query technologie: " + query );
		List<String> TechList = dummyDB.getTechList( query );
		System.out.println( TechList.toString() );
		return TechList.toString();
	}	
}
