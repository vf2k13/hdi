<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Ejemplo Spring MVC Autocomplete con JQuery &amp; JSON</title>

	<style>
/* Comentario en bloque estilo*/
	   body {
		 font-family: Calibri;
	    }
	   table {
		 border: 1px solid;
		 border-spacing:  5px;				
/*		 border-collapse: collapse; */
		 border-collapse: separate; 
	    }
	   td {
		 border: 1px solid;
	    }
	   th {
		 text-align: left;
	   }
	</style>
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />       
     
	<script type="text/javascript" 
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" 
		src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>  	
</head>
<body>
<!-- Comentario HTML  -->  

<h2>Ejemplo Spring MVC Autocomplete con JQuery &amp; JSON</h2>
  
  <form:form method="post" action="save.html" modelAttribute="userForm">
  <table cellspacing="3" cellpadding="3">
	<tr>
		<th>Name</th>
		<td><form:input path="name" /></td>
	</tr>
	<tr>
		<th>Country</th>
		<td><form:input path="country" id="country" /></td>
	</tr>
	<tr>
		<th>Technologies</th>
		<td><form:input path="technologies" id="technologies" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" >
			<input type="submit" value="Save" />
			&nbsp;&nbsp;
			<input type="reset" value="Reset" />
		</td>
	</tr>
  </table>	
  <br />	
  </form:form>

<script type="text/javascript"> 

// Comentario en bloque script
function split( val ) {     
	return val.split( /,\s*/ ); 
  } 
  
function extractLast( term ) {     
	return split( term ).pop(); 
  }   
  
$(document).ready( function() { 
	
//	alert('${pageContext.request.contextPath}/get_country_list.html');
	
	$( "#country" ).autocomplete({
		//Indicamos la fuente de donde se va a hacer el autocompletado.
		//Hacemos una petición a /get_country_list.htm para que nos devuelva
		//	la lista con los países 	
		source: '${pageContext.request.contextPath}/get_country_list.html' 
//		source:	["Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands"]

	}); // #country-autocomplete

	$( "#technologies" ).autocomplete({ 
		source: function( request, response ) { 
			$.getJSON( "${pageContext.request.contextPath}/get_tech_list.html", 
					   { term : extractLast( request.term ) }, response ); 							
	   }, // function
	// Eventos en el autocomplete:
    // Evita llamar al autocomplete cuando se hace backspace	   
	search: function () { 
		// custom minLength 
		 var term = extractLast( this.value ); 
		 if ( term.length < 1 ) { 
			 return false; // No ejecuta el autocomplete	 
		 }
	 }, 	
	 focus: function () { 
		// prevent value inserted on focus  
		 return false; // No ejecuta el autocomplete
	 },
	 select: function( event, ui ) { 
		 var terms = split( this.value );  
		// remove the current input 
		 terms.pop(); 
		// add the selected item 
		 terms.push( ui.item.value ); 
		// add placeholder to get the comma-and-space at the end 
		 terms.push( "" ); 
		 this.value = terms.join( ", " ); 
		 return false; // No ejecuta el autocomplete
	   }
    }); // #technologies-autocomplete
    
}); // document-ready-function

</script>   
</body>
</html>